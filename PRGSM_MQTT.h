

/*  ------------------------PRGSM_MQTT.h v 1.0.0----------------------------





*/





#ifndef PRGSM_MQTT_h
#define PRGSM_MQTT_h

//Arduino toolchain header, version dependent
#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif
#include "pins_arduino.h"

#include <avr/pgmspace.h>






#define ISA_GSM_NETWORK_APN         APN
#define ISA_GSM_SERVER              ip
#define ISA_GSM_PORT                port

#define ISA_GSM_CONNECT_MODE 1

#define ISA_GSM_USERID              USER
#define ISA_GSM_PASS                PASS


#define MESSAGE_BUFFER_LENGTH       300
#define RETURN_DATA_BUFFER_LENGTH   100

#if defined (__AVR_ATmega2560__)

#define ISASerial Serial
#define DataPort  Serial1
#define gsmSerial Serial3

#ifndef ISASerial_needed
#define ISASerial_needed 0
#endif




#else

#define ISASerial Serial
#define gsmSerial Serial1
#define DataPort Serial

#ifndef ISASerial_needed
#define ISASerial_needed 0
#endif


#endif





class PRGSM
{
  public:
    String ip, port;
    String APN, USER = "", PASS = "";
    char gsmData[MESSAGE_BUFFER_LENGTH];
    char gsmCapturedResponse[RETURN_DATA_BUFFER_LENGTH];
    int data_len;
    String date_server, time_server;
    String IMEI;
    char LINECOUNT_NORMAL = 3;
    char LINECOUNT_RESPONSE = 5;
    char LINECOUNT_EXTENDED = 6;
    //GLOBALS
    char gsmGlobalStage = 0;        // FLAG THAT KEEPS RECORD OF THE LAST STAGE EXECUTED - can take values from 0,1 and 2 .. Increments from 0 to 1 and 1 to 2 on successful stage completion
                                    // but will not increment from 2 even if stage 2 was successful. Incase one wants to check if stage 2 was cleared successfully , use gsmGlobalStageClear.
    char gsmGlobalErrorCount = 0;   // NO. of consecutive stage failures, becomes zero if even one stage is cleared. Can be used to monitor number of UPLOAD failures.
                                    // One UPLOAD failure may be 1 , 2 or 3 depending on how many stages failed.With help of gsmGlobalStage , no of upload failures can be determined.
    char gsmGlobalStageClear = 0;   // FLAG THAT INDICATES IF UPLOAD TO SEVERVER WAS SUCCESSFUL , i.e. becomes 1 from zero only if Stage 2 was successfully completed , i.e. upload was complete.

    char GSMModuleInfo = 1;         // by default it is SIM900, its value is 1, For SIM800 its value is 2.




    //PRGSM();
    char time_initialize();
    char time_info();
    char RSSI();
    char setAPN();
    char get_IMEI();
    char ISAgsmInit();
    void ISAgsmClose();
    void ISAgsmShut();
    char ISAgsmConnect();
    char checkConnectionStatus();
    char enterCommandMode();
    char exitCommandMode();

  private:

   
    char find_text(char* needle, char* haystack);
    int findIndexOf(const char *array, size_t size, char c, int startFrom = 0);
    void gsmSerialInputBufferFlush();
    void ISAgsmSend(String cmd);
    void ISAgsmSendwoCRLF(String cmd);
    void ISAgsmSendRaw(char *data, int data_length = 0);
    char readResponse(char timeout, uint8_t lineNumber = 0, char* expectedResponse = "OK", char* errorResponse = "ERROR");

};




#ifndef GSM_MQTT_H_
#define GSM_MQTT_H_





#define UART_BUFFER_LENGTH 300    //Maximum length allowed for UART data
#define TOPIC_BUFFER_LENGTH 50    //Maximum length allowed Topic
#define MESSAGE_BUFFER_LENGTH 250  //Maximum length allowed data

// ######################################################################################################################
#define CONNECT     1   //Client request to connect to Server                Client          Server
#define CONNACK     2   //Connect Acknowledgment                             Server/Client   Server/Client
#define PUBLISH     3   //Publish message                                    Server/Client   Server/Client
#define PUBACK      4   //Publish Acknowledgment                             Server/Client   Server/Client
#define PUBREC      5   //Publish Received (assured delivery part 1)         Server/Client   Server/Client
#define PUBREL      6   //Publish Release (assured delivery part 2)          Server/Client   Server/Client
#define PUBCOMP     7   //Publish Complete (assured delivery part 3)         Server/Client   Server/Client
#define SUBSCRIBE   8   //Client Subscribe request                           Client          Server
#define SUBACK      9   //Subscribe Acknowledgment                           Server          Client
#define UNSUBSCRIBE 10  //Client Unsubscribe request                         Client          Server
#define UNSUBACK    11  //Unsubscribe Acknowledgment                         Server          Client
#define PINGREQ     12  //PING Request                                       Client          Server
#define PINGRESP    13  //PING Response                                      Server          Client
#define DISCONNECT  14  //Client is Disconnecting                            Client          Server

// QoS value bit 2 bit 1 Description
//   0       0       0   At most once    Fire and Forget         <=1
//   1       0       1   At least once   Acknowledged delivery   >=1
//   2       1       0   Exactly once    Assured delivery        =1
//   3       1       1   Reserved
#define DUP_Mask      8   // Duplicate delivery   Only for QoS>0
#define QoS_Mask      6   // Quality of Service
#define QoS_Scale     2   // (()&QoS)/QoS_Scale
#define RETAIN_Mask   1   // RETAIN flag

#define User_Name_Flag_Mask  128
#define Password_Flag_Mask   64
#define Will_Retain_Mask     32
#define Will_QoS_Mask        24
#define Will_QoS_Scale       8
#define Will_Flag_Mask       4
#define Clean_Session_Mask   2

#define DISCONNECTED          0
#define CONNECTED             1
#define NO_ACKNOWLEDGEMENT  255

class GSM_MQTT
{
  public:
    volatile bool TCP_Flag = false;
    volatile char GSM_ReplyFlag;
    volatile bool pingFlag = false;
    volatile char tcpATerrorcount = 0;
    volatile bool MQTT_Flag = false;
    volatile int ConnectionAcknowledgement = NO_ACKNOWLEDGEMENT ;
    volatile int PublishIndex = 0;
    char Topic[TOPIC_BUFFER_LENGTH];
    volatile int TopicLength = 0;
    char Message[MESSAGE_BUFFER_LENGTH];
    volatile int MessageLength = 0;
    volatile int MessageFlag = false;
    volatile char modemStatus = 0;
    volatile uint32_t index = 0;
    volatile uint32_t length = 0, lengthLocal = 0;


    GSM_MQTT(unsigned long KeepAlive);
    void connect(char *ClientIdentifier, char UserNameFlag, char PasswordFlag, char *UserName, char *Password, char CleanSession, char WillFlag, char WillQoS, char WillRetain, char *WillTopic, char *WillMessage);
    void publish(char DUP, char Qos, char RETAIN, unsigned int MessageID, char *Topic, char *Message);
    void subscribe(char DUP, unsigned int MessageID, char *SubTopic, char SubQoS);
    void unsubscribe(char DUP, unsigned int MessageID, char *SubTopic);
    void disconnect(void);
    bool available(void);

    void AutoConnect(void);
    void OnConnect(void);
    void OnMessage(char *Topic, int TopicLength, char *Message, int MessageLength);

    void publishACK(unsigned int MessageID);
    void publishREC(unsigned int MessageID);
    void publishREL(char DUP, unsigned int MessageID);
    void publishCOMP(unsigned int MessageID);

    void printMessageType(uint8_t Message);
    void printConnectAck(uint8_t Ack);

    void _ping(void);

  private:
    volatile unsigned int _LastMessaseID = 0;
    volatile char _ProtocolVersion = 3;
    volatile unsigned long _PingPrevMillis = 0;
    volatile char _tcpStatus = 0;
    volatile char _tcpStatusPrev = 0;
    volatile unsigned long _KeepAliveTimeOut;

    void _sendUTFString(char *string);
    void _sendLength(int len);
    unsigned int _generateMessageID(void);
};
#endif /* GSM_MQTT_H_ */






#endif