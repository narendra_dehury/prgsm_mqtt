

/*	------------------------------- PRGSM_MQTT.cpp v 1.1.0------------------------------------  









*/



#include "PRGSM_MQTT.h"
#include "Arduino.h"
#include <SoftwareSerial.h>
#include <avr/pgmspace.h>

#define ISASerial_needed 1

#define NO_OF_OPERATORS 7

char* OPERATORS[NO_OF_OPERATORS] = {"BSNL", "airtel", "AIRTEL", "VODAFONE", "vodafone", "AIRCEL", "DOCOMO"} ;

char* APNS[NO_OF_OPERATORS] = {"bsnlnet", "airtelgprs.com", "airtelgprs.com", "portalnmms", "portalnmms", "aircelgprs", "TATA.DOCOMO.INTERNET"} ;

// This function helps to find if a text contains a phrase

char PRGSM::find_text(char* needle, char* haystack) 
{
  if(strstr(haystack, needle)!= NULL)
  	return 1;
  else
  	return 0;
}



int PRGSM::findIndexOf(const char *array, size_t size, char c, int startFrom)
{
  if(startFrom!=0) {
    for(size_t i = startFrom; i< size; i++)
    {
      if (array[i] == c)
        return (int)i;
     }
  }
  else {
    for (size_t i = 0; i < size; i++)
    {
        if (array[i] == c)
            return (int)i;
    }
  }
    return -1;
}



void PRGSM::gsmSerialInputBufferFlush()
{
	while(gsmSerial.available()) gsmSerial.read();
}


void PRGSM::ISAgsmSend(String cmd)
{
	gsmSerialInputBufferFlush();	// clear buffer again
	gsmSerial.println(cmd);

	// Seems like the delay is no more required for stability , instead it causes problem if the cmd is too long
	//delay(500);	// for stability

	gsmSerial.flush();	// wait for transmission to complete VERY IMPORTANT as ISASerial.print is handled by interrupt 
						//and control passes to next statement which sends another cdm before the previous is processed

	#if ISASerial_needed
		ISASerial.print(F("GSM Command : "));
		ISASerial.println(cmd);
	#endif	


}


void PRGSM::ISAgsmSendwoCRLF(String cmd)
{
	gsmSerial.print(cmd);
	gsmSerial.flush();	// wait for transmission to complete VERY IMPORTANT as ISASerial.print is handled by interrupt 
						//and control passes to next statement which sends another cdm before the previous is processed.
}



void PRGSM::ISAgsmSendRaw(char *data,int data_length)
{
	if(!data_length) data_length=strlen(data);
	for(int i=0;i<data_length;i++)
		gsmSerial.write(data[i]);
}



char PRGSM::readResponse(char timeout, uint8_t lineNumber, char* expectedResponse, char* errorResponse) {
  
  unsigned long millis_stamp = millis();
  uint16_t charCount = 0;
  uint8_t lineCount = 0;
  char returnFlag = 0;
  
  memset(gsmCapturedResponse, 0, sizeof(gsmCapturedResponse));
  memset(gsmData, 0, sizeof(gsmData));
  
  uint16_t Start =0, End = 0; 
  gsmSerial.flush();
  
  while(1) {
    if(gsmSerial.available()) {
      char ch = gsmSerial.read();
      if(ch == '\n') {
        lineCount++;
        //ISASerial.println(lineCount);
        if(lineNumber) {
          if(lineCount == lineNumber - 1) {
            Start = charCount + 1;

            #if ISASerial_needed
            ISASerial.println();
            ISASerial.println(F("Start of Recording"));
            #endif
            
          }
          else if(lineCount == lineNumber) {
            End = charCount;

            #if ISASerial_needed
            ISASerial.println(F("End of Recording"));
            #endif
          }
        }
      }

      #if ISASerial_needed
      ISASerial.print(ch);
      #endif

      gsmData[charCount++] = ch;
    }
    if(!find_text(errorResponse, gsmData)) {
      if(find_text(expectedResponse, gsmData)) {

        #if ISASerial_needed
        ISASerial.println();
        ISASerial.println(F("@ readResponse() => RESPONSE MATCHED!!"));
        #endif

        returnFlag = 1;
        break;
      }
    }
    else {

      #if ISASerial_needed
      ISASerial.println();
      ISASerial.println(F("@ readResponse() => CAUGHT ERROR!!"));
      #endif

      returnFlag = 0;
      break;
    }
    if((millis()-millis_stamp) > timeout*1000) {

      #if ISASerial_needed
      ISASerial.println(F("@ readResponse() => Readline Timed out. Partial Line Received : "));
      ISASerial.println(gsmData);
      #endif

      break;
    }
  }
  if(lineNumber && (Start!=End)) {

    #if ISASerial_needed
    ISASerial.println(F("RESPONSE CAPTURED!!"));
    #endif

    for(uint16_t i = 0; i < (End - Start - 1); i++) {
      gsmCapturedResponse[i] = gsmData[Start + i];

      #if ISASerial_needed
      ISASerial.print(gsmCapturedResponse[i]);
      #endif
    }

    #if ISASerial_needed
    ISASerial.println();
    #endif
  }
  else {

    #if ISASerial_needed
    ISASerial.println(F("@ readResponse() => Response Capture Disabled!!"));
    #endif
  }
  delay(1);
  gsmSerialInputBufferFlush();
  delay(1);
  return returnFlag;
}







char PRGSM::ISAgsmInit()
{
  gsmSerial.end();
  gsmSerial.begin(9600);

  char local_error_count = 0;
  
  enterCommandMode();

  for(char gsmLocalStage=0;gsmLocalStage<5;)
  {
    switch(gsmLocalStage)
    {
      case 0:
        ISAgsmSend("AT");
        if(readResponse(1))gsmLocalStage++;
        else local_error_count++;
        break;
      
      case 1: 
        ISAgsmSend("AT+CPIN?");
        if(readResponse(1))gsmLocalStage++;
        else local_error_count++;
        break;

      case 2: 
        ISAgsmSend("AT+CREG?");
        if(readResponse(1))gsmLocalStage++;
        else local_error_count++;
        break;
      
      case 3: 
        ISAgsmSend("AT+CGATT?");
        if(readResponse(1))gsmLocalStage++;
        else local_error_count++;
        break;
      
      case 4: 
        char l_count = 5;
        ISAgsmSend("AT+CGDCONT?");

        if(readResponse(1))gsmLocalStage++;
        else local_error_count++;
    }

    if(gsmLocalStage==5) {

      #if ISASerial_needed
      ISASerial.print(F("local stage: "));
      ISASerial.println(int(gsmLocalStage));
      #endif

      return 1;
    }

    if(local_error_count>1) {

      #if ISASerial_needed
      ISASerial.print(F("local stage: "));
      ISASerial.println(int(gsmLocalStage));
      #endif

      return 0;
    }
  }
  return 0;
}


void PRGSM::ISAgsmClose()
{
	gsmSerialInputBufferFlush();	// clear input ISASerial buffer
	ISAgsmSend("AT+CIPCLOSE"); // close TCP/IP session
    readResponse(15, 0, "CLOSE OK");	// check for "CLOSE OK"
}

void PRGSM::ISAgsmShut()
{
	gsmSerialInputBufferFlush();	// clear input ISASerial buffer
	ISAgsmSend("AT+CIPSHUT"); // close TCP/IP session
    readResponse(15, 0, "SHUT OK");	// check for "CLOSE OK"
}



char PRGSM::ISAgsmConnect()
{
  char local_error_count = 0;

  for(char gsmLocalStage = 0; gsmLocalStage < 7;)
  {
    switch(gsmLocalStage)
    {
       case 0:
       ISAgsmSend("AT+CIPSHUT");
       if(readResponse(15,0,"SHUT OK"))gsmLocalStage++;

       else local_error_count++;
       break;

       case 1:
       ISAgsmSend("AT+CIPMODE=1");
       if(readResponse(1))gsmLocalStage++;
       else local_error_count++;
       break;

       case 2:
       ISAgsmSend("AT+CIPMUX=0");
       if(readResponse(2))gsmLocalStage++;
       else local_error_count++;
       break;

       case 3:
       ISAgsmSend("AT+CSTT=\"" + String(ISA_GSM_NETWORK_APN) + "\",\"" + ISA_GSM_USERID + "\",\"" + ISA_GSM_PASS + "\"");
       if(readResponse(3))gsmLocalStage++;
       else local_error_count++;
       break;

       case 4:
       ISAgsmSend("AT+CIICR");  //takes time here , so we need to include wdt_reset(); routine here
       if(readResponse(25))gsmLocalStage++;
       else local_error_count++;
       break;

       case 5:
       ISAgsmSend("AT+CIFSR");
       if(readResponse(3,0,"."))gsmLocalStage++;
       else local_error_count++;
       break;

       case 6:
       ISAgsmSend("AT+CIPSTART=\"TCP\",\"" + String(ISA_GSM_SERVER) + "\",\"" + String(ISA_GSM_PORT) +"\"" );
       if(readResponse(15,0,"CONNECT"))gsmLocalStage++;
       else local_error_count++;
       break;

    }

    if(gsmLocalStage==7) {

      #if ISASerial_needed
      ISASerial.print(F("local stage: "));
      ISASerial.println(int(gsmLocalStage));  
      #endif
      return 1;
    }
    if(local_error_count>0) {

      #if ISASerial_needed
      ISASerial.print(F("local stage: "));
      ISASerial.println(int(gsmLocalStage));
      #endif
      return 0;
    }
  }
  return 0;
}





char PRGSM::time_initialize() {

  ISAgsmSend("AT+CTZU=1");
  if (readResponse(1, 0, "OK"));


}




char PRGSM::time_info() {


    ISAgsmSend("AT+CCLK?");

  if (readResponse(1, 3, "OK"))
  {
  
    //ISASerial.println("response");
    //ISASerial.println(gsmCapturedResponse);
    int sizeOfData = sizeof(gsmCapturedResponse);
    uint8_t Start = findIndexOf(gsmCapturedResponse, sizeOfData, '\"') + 1;
    uint8_t End = findIndexOf(gsmCapturedResponse, sizeOfData, '\"', Start + 1);

    char lv_arr[24];
    memset(lv_arr, 0, sizeof(lv_arr));
    for(uint8_t i = 0; i < (End-Start); i++) {
      lv_arr[i] = gsmCapturedResponse[Start + i];

      #if ISASerial_needed
      ISASerial.print(lv_arr[i]);
      #endif
    }
   

    Start =  0;
    sizeOfData = sizeof(lv_arr);
    End = findIndexOf(lv_arr, sizeOfData, ',', Start + 1) + 1;

    
    char temp_date[12];
    for(uint8_t i = 0; i < (End-Start); i++)
    {
      temp_date[i] = lv_arr[Start + i];

      #if ISASerial_needed
      ISASerial.print(temp_date[i]);
      #endif
    }

    #if ISASerial_needed
    ISASerial.println();
    #endif
    

    char temp_time[12];
    memset(temp_time, 0, sizeof(temp_time));

    Start = End + 1;
    End = findIndexOf(lv_arr, sizeOfData, '+', Start + 1);
    for(uint8_t i = 0; i < (End-Start); i++)
    {
      temp_time[i] = lv_arr[Start + i];

      #if ISASerial_needed
      ISASerial.print(temp_time[i]);
      #endif
    }

    #if ISASerial_needed
    ISASerial.println();
    #endif

    time_server = String(temp_time);

    char date_separater = '/'; 
    char dd[3] ,mm[3] ,yy[3] ;

    memset(dd, 0, sizeof(dd));
    memset(mm, 0, sizeof(mm));
    memset(yy, 0, sizeof(yy));

    sizeOfData = sizeof(temp_date);
    Start = 0;
    End = findIndexOf(temp_date, sizeOfData, date_separater);
    for(uint8_t i = 0; i < (End-Start); i++)
    {
      yy[i] = temp_date[Start + i];

    }

    Start = End+1;
    End = findIndexOf(temp_date, sizeOfData, date_separater, Start + 1);
    for(uint8_t i = 0; i < (End-Start); i++)
    {
      mm[i] = temp_date[Start + i];
    }


   Start = End+1;
    End = findIndexOf(temp_date, sizeOfData, ',', Start + 1);
    for(uint8_t i = 0; i < (End-Start); i++)
    {
      dd[i] = temp_date[Start + i];
    }

    // ISASerial.println(dd);
    // ISASerial.println(mm);
    // ISASerial.println(yy);


    memset(temp_date, 0, sizeof(temp_date));

    strcpy(temp_date, dd);
    temp_date[2] = '-';
    strcat(temp_date, mm);
    temp_date[5] = '-';
    temp_date[6] = '2';
    temp_date[7] = '0';
    strcat(temp_date, yy);
    //strcpy(temp_date, '-');
    //ISASerial.println(temp_date);

    date_server = temp_date;

    #if ISASerial_needed
    ISASerial.print(F("Date: "));
    ISASerial.println(date_server);
    ISASerial.print(F("Time: "));
    ISASerial.println(time_server);
    #endif

  return 1;

    }
    else {
      #if ISASerial_needed
      ISASerial.println(F("CLOCK READ ERROR !"));
      ISASerial.println(F("-------"));
      #endif
      return 0;
    }

}





char PRGSM::RSSI() {


   ISAgsmSend("AT+CSQ");

  if (readResponse(1, 3, "OK"))
  {
  
    // ISASerial.println("response");
    // ISASerial.println(gsmCapturedResponse);
    int sizeOfData = sizeof(gsmCapturedResponse);
    uint8_t Start = findIndexOf(gsmCapturedResponse, sizeOfData, ':') + 1;
    uint8_t End = findIndexOf(gsmCapturedResponse, sizeOfData, ',');

    char lv_arr[4];
    for(uint8_t i = 0; i < (End-Start); i++) {
      lv_arr[i] = gsmCapturedResponse[Start + i];

      #if ISASerial_needed
      ISASerial.print(lv_arr[i]);
      #endif
    }

    char lv_rssi = String(lv_arr).toInt();
    
    #if ISASerial_needed

    ISASerial.println();
    ISASerial.println(F("RSSI : "));
    ISASerial.println(int(lv_rssi));
    if(lv_rssi > 19) ISASerial.println(F("Signal Strength is Excellent!!"));
    else if(lv_rssi > 15) ISASerial.println(F("Signal Strength is Good!!"));
    else if(lv_rssi > 10) ISASerial.println(F("Signal Strength is just OK!!"));
    else ISASerial.println(F("Signal Strength is Poor!!"));
    
    #endif

    return lv_rssi;
  }
  else {

    #if ISASerial_needed
    ISASerial.println(F("RSSI READ ERROR !"));
    ISASerial.println(F("-------"));
    #endif

    return 0;
  }
}


char PRGSM::setAPN()
{

  if (APN == "");
  else
  {
    #if ISASerial_needed
    ISASerial.print(F("APN ALREADY SET--->"));
    ISASerial.println(APN) ;
    #endif
    return 1;
  }   

  ISAgsmSend("AT+CSPN?");

  if (readResponse(1, 3, "OK"))
  {

    // ISASerial.println(F("Response"));
    // ISASerial.println(gsmCapturedResponse);
    int sizeOfData = sizeof(gsmCapturedResponse);


    uint8_t Start = findIndexOf(gsmCapturedResponse, sizeOfData, '\"') + 1;
    uint8_t End = findIndexOf(gsmCapturedResponse, sizeOfData, '\"', Start + 1);


    char lv_arr[15];
    for(uint8_t i = 0; i < (End-Start); i++) {
      lv_arr[i] = gsmCapturedResponse[Start + i];

      #if ISASerial_needed
      ISASerial.print(lv_arr[i]);
      #endif
    }

    for(uint8_t i = 0; i < NO_OF_OPERATORS; i++)
    {
      if(find_text(OPERATORS[i], lv_arr))
      {
        
        APN = String(APNS[i]);

        #if ISASerial_needed
        ISASerial.println();
        ISASerial.println(F("Match Found!! "));
        ISASerial.println(APN);
        #endif

        break;
      }
    }

    if(APN != "")
    {
      #if ISASerial_needed
      ISASerial.print(F("New APN: "));
      ISASerial.println(APN);
      #endif
      return 1;
    }
    else
    {
      #if ISASerial_needed
      ISASerial.println(F("Operator Not Recognized!!"));
      #endif
      return 0;
    }

  }

}



char PRGSM::get_IMEI() {

  ISAgsmSend("AT+GSN");
  if (readResponse(1, 3, "OK"))
  {
    // ISASerial.println(F("Response"));
    // ISASerial.println(gsmCapturedResponse);
    int sizeOfData = sizeof(gsmCapturedResponse);
    IMEI = String(gsmCapturedResponse);

    #if ISASerial_needed
    ISASerial.print(F("IMEI: "));
    ISASerial.println(IMEI);
    #endif

    return 1;
  }
  else {

    #if ISASerial_needed
    ISASerial.println(F("IMEI READ ERROR !"));
    ISASerial.println(F("-------"));
    #endif

    return 0;
  }

}





char PRGSM::enterCommandMode()
{
  delay(1000);
  ISAgsmSendRaw("+++");
  delay(500);  
}



char PRGSM::exitCommandMode()
{
  ISAgsmSend("ATO");
  if (readResponse(4, 0, "CONNECT"))
  {
    #if ISASerial_needed
    ISASerial.println(F("EXITED FROM COMMAND MODE!!"));
    #endif
    return 1;
  }
  else
  {
    #if ISASerial_needed
    ISASerial.println(F("ERROR Exiting COMMAND MODE!!"));
    #endif
    return 0;
  }

}




char PRGSM::checkConnectionStatus()
{
  enterCommandMode();
  ISAgsmSend("AT+CIPSTATUS");
  if (readResponse(4, 0, "CONNECT"))
  {
    #if ISASerial_needed
    ISASerial.println(F("CONNECTED TO SERVER"));
    #endif
    gsmSerialInputBufferFlush();
    exitCommandMode();
    return 1;    
  }
  else
  {
    #if ISASerial_needed
    ISASerial.println(F("NOT CONNECTED TO ANY SERVER"));
    #endif
  }
  return 0;
}























/*
  MQTT.h - Library for GSM MQTT Client.
  Created by Nithin K. Kurian, Dhanish Vijayan, Elementz Engineers Guild Pvt. Ltd, July 2, 2016.
  Released into the public domain.
*/


extern uint8_t GSM_Response;


extern String MQTT_HOST;
extern String MQTT_PORT;


uint8_t GSM_Response = 0;
unsigned long previousMillis = 0;
//char inputString[UART_BUFFER_LENGTH];         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete

GSM_MQTT::GSM_MQTT(unsigned long KeepAlive)
{
  _KeepAliveTimeOut = KeepAlive;
}


void GSM_MQTT::_ping(void)
{


  if (pingFlag == true)
  {
    unsigned long currentMillis = millis();
    if ((currentMillis - _PingPrevMillis ) >= (_KeepAliveTimeOut) * 1000)
    {
      // save the last time you blinked the LED
      _PingPrevMillis = currentMillis;
      gsmSerial.print(char(PINGREQ * 16));
      _sendLength(0);

      #if ISASerial_needed
      ISASerial.println(F("Inside"));
      #endif
    }
  }
}
void GSM_MQTT::_sendUTFString(char *string)
{
  int localLength = strlen(string);
  gsmSerial.print(char(localLength / 256));
  gsmSerial.print(char(localLength % 256));
  gsmSerial.print(string);
}
void GSM_MQTT::_sendLength(int len)
{
  bool  length_flag = false;
  while (length_flag == false)
  {
    if ((len / 128) > 0)
    {
      gsmSerial.print(char(len % 128 + 128));
      len /= 128;
    }
    else
    {
      length_flag = true;
      gsmSerial.print(char(len));
    }
  }
}
void GSM_MQTT::connect(char *ClientIdentifier, char UserNameFlag, char PasswordFlag, char *UserName, char *Password, char CleanSession, char WillFlag, char WillQoS, char WillRetain, char *WillTopic, char *WillMessage)
{
  ConnectionAcknowledgement = NO_ACKNOWLEDGEMENT ;
  gsmSerial.print(char(CONNECT * 16 ));
  char ProtocolName[7] = "MQIsdp";
  int localLength = (2 + strlen(ProtocolName)) + 1 + 3 + (2 + strlen(ClientIdentifier));
  if (WillFlag != 0)
  {
    localLength = localLength + 2 + strlen(WillTopic) + 2 + strlen(WillMessage);
  }
  if (UserNameFlag != 0)
  {
    localLength = localLength + 2 + strlen(UserName);

    if (PasswordFlag != 0)
    {
      localLength = localLength + 2 + strlen(Password);
    }
  }
  _sendLength(localLength);
  _sendUTFString(ProtocolName);
  gsmSerial.print(char(_ProtocolVersion));
  gsmSerial.print(char(UserNameFlag * User_Name_Flag_Mask + PasswordFlag * Password_Flag_Mask + WillRetain * Will_Retain_Mask + WillQoS * Will_QoS_Scale + WillFlag * Will_Flag_Mask + CleanSession * Clean_Session_Mask));
  gsmSerial.print(char(_KeepAliveTimeOut / 256));
  gsmSerial.print(char(_KeepAliveTimeOut % 256));
  _sendUTFString(ClientIdentifier);
  if (WillFlag != 0)
  {
    _sendUTFString(WillTopic);
    _sendUTFString(WillMessage);
  }
  if (UserNameFlag != 0)
  {
    _sendUTFString(UserName);
    if (PasswordFlag != 0)
    {
      _sendUTFString(Password);
    }
  }
}
void GSM_MQTT::publish(char DUP, char Qos, char RETAIN, unsigned int MessageID, char *Topic, char *Message)
{
  gsmSerial.print(char(PUBLISH * 16 + DUP * DUP_Mask + Qos * QoS_Scale + RETAIN));
  int localLength = (2 + strlen(Topic));
  if (Qos > 0)
  {
    localLength += 2;
  }
  localLength += strlen(Message);
  _sendLength(localLength);
  _sendUTFString(Topic);
  if (Qos > 0)
  {
    gsmSerial.print(char(MessageID / 256));
    gsmSerial.print(char(MessageID % 256));
  }
  gsmSerial.print(Message);
}
void GSM_MQTT::publishACK(unsigned int MessageID)
{
  gsmSerial.print(char(PUBACK * 16));
  _sendLength(2);
  gsmSerial.print(char(MessageID / 256));
  gsmSerial.print(char(MessageID % 256));
}
void GSM_MQTT::publishREC(unsigned int MessageID)
{
  gsmSerial.print(char(PUBREC * 16));
  _sendLength(2);
  gsmSerial.print(char(MessageID / 256));
  gsmSerial.print(char(MessageID % 256));
}
void GSM_MQTT::publishREL(char DUP, unsigned int MessageID)
{
  gsmSerial.print(char(PUBREL * 16 + DUP * DUP_Mask + 1 * QoS_Scale));
  _sendLength(2);
  gsmSerial.print(char(MessageID / 256));
  gsmSerial.print(char(MessageID % 256));
}

void GSM_MQTT::publishCOMP(unsigned int MessageID)
{
  gsmSerial.print(char(PUBCOMP * 16));
  _sendLength(2);
  gsmSerial.print(char(MessageID / 256));
  gsmSerial.print(char(MessageID % 256));
}

void GSM_MQTT::subscribe(char DUP, unsigned int MessageID, char *SubTopic, char SubQoS)
{
  gsmSerial.print(char(SUBSCRIBE * 16 + DUP * DUP_Mask + 1 * QoS_Scale));
  int localLength = 2 + (2 + strlen(SubTopic)) + 1;
  _sendLength(localLength);
  gsmSerial.print(char(MessageID / 256));
  gsmSerial.print(char(MessageID % 256));
  _sendUTFString(SubTopic);
  gsmSerial.print(SubQoS);

}
void GSM_MQTT::unsubscribe(char DUP, unsigned int MessageID, char *SubTopic)
{
  gsmSerial.print(char(UNSUBSCRIBE * 16 + DUP * DUP_Mask + 1 * QoS_Scale));
  int localLength = (2 + strlen(SubTopic)) + 2;
  _sendLength(localLength);

  gsmSerial.print(char(MessageID / 256));
  gsmSerial.print(char(MessageID % 256));

  _sendUTFString(SubTopic);
}
void GSM_MQTT::disconnect(void)
{
  gsmSerial.print(char(DISCONNECT * 16));
  _sendLength(0);
  pingFlag = false;
}
//Messages
const char CONNECTMessage[] PROGMEM  = {"Client request to connect to Server\r\n"};
const char CONNACKMessage[] PROGMEM  = {"Connect Acknowledgment\r\n"};
const char PUBLISHMessage[] PROGMEM  = {"Publish message\r\n"};
const char PUBACKMessage[] PROGMEM  = {"Publish Acknowledgment\r\n"};
const char PUBRECMessage[] PROGMEM  = {"Publish Received (assured delivery part 1)\r\n"};
const char PUBRELMessage[] PROGMEM  = {"Publish Release (assured delivery part 2)\r\n"};
const char PUBCOMPMessage[] PROGMEM  = {"Publish Complete (assured delivery part 3)\r\n"};
const char SUBSCRIBEMessage[] PROGMEM  = {"Client Subscribe request\r\n"};
const char SUBACKMessage[] PROGMEM  = {"Subscribe Acknowledgment\r\n"};
const char UNSUBSCRIBEMessage[] PROGMEM  = {"Client Unsubscribe request\r\n"};
const char UNSUBACKMessage[] PROGMEM  = {"Unsubscribe Acknowledgment\r\n"};
const char PINGREQMessage[] PROGMEM  = {"PING Request\r\n"};
const char PINGRESPMessage[] PROGMEM  = {"PING Response\r\n"};
const char DISCONNECTMessage[] PROGMEM  = {"Client is Disconnecting\r\n"};

void GSM_MQTT::printMessageType(uint8_t Message)
{
  switch (Message)
  {
    case CONNECT:
      {
        int k, len = strlen_P(CONNECTMessage);
        char myChar;
        for (k = 0; k < len; k++)
        {
          myChar =  pgm_read_byte_near(CONNECTMessage + k);

          #if ISASerial_needed
          ISASerial.print(myChar);
          #endif
        }
        break;
      }
    case CONNACK:
      {
        int k, len = strlen_P(CONNACKMessage);
        char myChar;
        for (k = 0; k < len; k++)
        {
          myChar =  pgm_read_byte_near(CONNACKMessage + k);

          #if ISASerial_needed
          ISASerial.print(myChar);
          #endif
        }
        break;
      }
    case PUBLISH:
      {
        int k, len = strlen_P(PUBLISHMessage);
        char myChar;
        for (k = 0; k < len; k++)
        {
          myChar =  pgm_read_byte_near(PUBLISHMessage + k);

          #if ISASerial_needed
          ISASerial.print(myChar);
          #endif

        }
        break;
      }
    case PUBACK:
      {
        int k, len = strlen_P(PUBACKMessage);
        char myChar;
        for (k = 0; k < len; k++)
        {
          myChar =  pgm_read_byte_near(PUBACKMessage + k);

          #if ISASerial_needed
          ISASerial.print(myChar);
          #endif

        }
        break;
      }
    case  PUBREC:
      {
        int k, len = strlen_P(PUBRECMessage);
        char myChar;
        for (k = 0; k < len; k++)
        {
          myChar =  pgm_read_byte_near(PUBRECMessage + k);

          #if ISASerial_needed
          ISASerial.print(myChar);
          #endif

        }
        break;
      }
    case PUBREL:
      {
        int k, len = strlen_P(PUBRELMessage);
        char myChar;
        for (k = 0; k < len; k++)
        {
          myChar =  pgm_read_byte_near(PUBRELMessage + k);

          #if ISASerial_needed
          ISASerial.print(myChar);
          #endif

        }
        break;
      }
    case PUBCOMP:
      {
        int k, len = strlen_P(PUBCOMPMessage );
        char myChar;
        for (k = 0; k < len; k++)
        {
          myChar =  pgm_read_byte_near(PUBCOMPMessage  + k);

          #if ISASerial_needed
          ISASerial.print(myChar);
          #endif

        }
        break;
      }
    case SUBSCRIBE:
      {
        int k, len = strlen_P(SUBSCRIBEMessage );
        char myChar;
        for (k = 0; k < len; k++)
        {
          myChar =  pgm_read_byte_near(SUBSCRIBEMessage  + k);

          #if ISASerial_needed
          ISASerial.print(myChar);
          #endif

        }
        break;
      }
    case SUBACK:
      {
        int k, len = strlen_P(SUBACKMessage );
        char myChar;
        for (k = 0; k < len; k++)
        {
          myChar =  pgm_read_byte_near(SUBACKMessage  + k);

          #if ISASerial_needed
          ISASerial.print(myChar);
          #endif

        }
        break;
      }
    case UNSUBSCRIBE:
      {
        int k, len = strlen_P(UNSUBSCRIBEMessage );
        char myChar;
        for (k = 0; k < len; k++)
        {
          myChar =  pgm_read_byte_near(UNSUBSCRIBEMessage  + k);

          #if ISASerial_needed
          ISASerial.print(myChar);
          #endif

        }
        break;
      }
    case UNSUBACK:
      {
        int k, len = strlen_P(UNSUBACKMessage );
        char myChar;
        for (k = 0; k < len; k++)
        {
          myChar =  pgm_read_byte_near(UNSUBACKMessage  + k);

          #if ISASerial_needed
          ISASerial.print(myChar);
          #endif

        }
        break;
      }
    case PINGREQ:
      {
        int k, len = strlen_P(PINGREQMessage);
        char myChar;
        for (k = 0; k < len; k++)
        {
          myChar =  pgm_read_byte_near(PINGREQMessage + k);

          #if ISASerial_needed
          ISASerial.print(myChar);
          #endif

        }
        break;
      }
    case PINGRESP:
      {
        int k, len = strlen_P(PINGRESPMessage);
        char myChar;
        for (k = 0; k < len; k++)
        {
          myChar =  pgm_read_byte_near(PINGRESPMessage + k);

          #if ISASerial_needed
          ISASerial.print(myChar);
          #endif

        }
        break;
      }
    case DISCONNECT:
      {
        int k, len = strlen_P(DISCONNECTMessage);
        char myChar;
        for (k = 0; k < len; k++)
        {
          myChar =  pgm_read_byte_near(DISCONNECTMessage + k);

          #if ISASerial_needed
          ISASerial.print(myChar);
          #endif

        }
        break;
      }
  }
}

//Connect Ack
const char ConnectAck0[] PROGMEM  = {"Connection Accepted\r\n"};
const char ConnectAck1[] PROGMEM  = {"Connection Refused: unacceptable protocol version\r\n"};
const char ConnectAck2[] PROGMEM  = {"Connection Refused: identifier rejected\r\n"};
const char ConnectAck3[] PROGMEM  = {"Connection Refused: server unavailable\r\n"};
const char ConnectAck4[] PROGMEM  = {"Connection Refused: bad user name or password\r\n"};
const char ConnectAck5[] PROGMEM  = {"Connection Refused: not authorized\r\n"};
void GSM_MQTT::printConnectAck(uint8_t Ack)
{
  switch (Ack)
  {
    case 0:
      {
        int k, len = strlen_P(ConnectAck0);
        char myChar;
        for (k = 0; k < len; k++)
        {
          myChar =  pgm_read_byte_near(ConnectAck0 + k);

          #if ISASerial_needed
          ISASerial.print(myChar);
          #endif

        }
        break;
      }
    case 1:
      {
        int k, len = strlen_P(ConnectAck1);
        char myChar;
        for (k = 0; k < len; k++)
        {
          myChar =  pgm_read_byte_near(ConnectAck1 + k);

          #if ISASerial_needed
          ISASerial.print(myChar);
          #endif

        }
        break;
      }
    case 2:
      {
        int k, len = strlen_P(ConnectAck2);
        char myChar;
        for (k = 0; k < len; k++)
        {
          myChar =  pgm_read_byte_near(ConnectAck2 + k);

          #if ISASerial_needed
          ISASerial.print(myChar);
          #endif

        }
        break;
      }
    case 3:
      {
        int k, len = strlen_P(ConnectAck3);
        char myChar;
        for (k = 0; k < len; k++)
        {
          myChar =  pgm_read_byte_near(ConnectAck3 + k);

          #if ISASerial_needed
          ISASerial.print(myChar);
          #endif

        }
        break;
      }
    case 4:
      {
        int k, len = strlen_P(ConnectAck4);
        char myChar;
        for (k = 0; k < len; k++)
        {
          myChar =  pgm_read_byte_near(ConnectAck4 + k);

          #if ISASerial_needed
          ISASerial.print(myChar);
          #endif

        }
        break;
      }
    case 5:
      {
        int k, len = strlen_P(ConnectAck5);
        char myChar;
        for (k = 0; k < len; k++)
        {
          myChar =  pgm_read_byte_near(ConnectAck5 + k);

          #if ISASerial_needed
          ISASerial.print(myChar);
          #endif

        }
        break;
      }
  }
}
unsigned int GSM_MQTT::_generateMessageID(void)
{
  if (_LastMessaseID < 65535)
  {
    return ++_LastMessaseID;
  }
  else
  {
    _LastMessaseID = 0;
    return _LastMessaseID;
  }
}

bool GSM_MQTT::available(void)
{
  return MQTT_Flag;
}
