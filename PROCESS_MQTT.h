
/*  ------------------------PROCESS_MQTT.h v 1.1.0----------------------------





*/





#include "PRGSM_MQTT.h"

PRGSM GSMObj;
extern GSM_MQTT MQTT;

  unsigned long pingInterval = 900000;
  unsigned long ping_time_stamp = millis();

void ISAgsmPIPE()
{
  // wait for out buffer to get empty , i.e. wait untill all gsmData gets sent out if ISASerial ports are already sending some gsmData
  ISASerial.flush();
  gsmSerial.flush();

  // Initialise at 9600 baud
   ISASerial.begin(9600);
   gsmSerial.begin(9600);

  ISASerial.println(F("PIPING MODE ACTIVE - type '$' to exit."));

  while(1)
  {
    if(ISASerial.available()) 
    {
      int byte = ISASerial.read();  // .read() returns an int
      if(byte =='$') break;
      gsmSerial.write(byte);
    }
    if(gsmSerial.available()) 
    {
      ISASerial.write(gsmSerial.read());
    }
  }

  ISASerial.println();
  ISASerial.println(F("PIPING MODE EXITED"));
}







char pingStatus()
{

  if(millis() - ping_time_stamp > pingInterval)
  {
    ping_time_stamp = millis();
    if(GSMObj.checkConnectionStatus())
    {
      return 1;
    }
    else 
    {
      return 0;
    }
  }
  return 2;
}



void setServer(String Ip, String Port)
{
  GSMObj.ip = Ip;
  GSMObj.port = Port; 
}


void processing(void)
{

  if (MQTT.TCP_Flag == false)
  {
    MQTT.MQTT_Flag = false;
    GSMObj.enterCommandMode();
    GSMObj.setAPN();
    GSMObj.ISAgsmInit();

    if(GSMObj.ISAgsmConnect()) {
      MQTT.TCP_Flag = true;
      MQTT.pingFlag = true;
      MQTT.AutoConnect();
    }
  }
  else
  {
    if(pingStatus())
    {
      MQTT.TCP_Flag = true;
    }
    else
      MQTT.TCP_Flag = false;    
  }
  
  MQTT._ping();
}

void serialEvent3()
{

  while (gsmSerial.available())
  {
    char inChar = (char)gsmSerial.read();


      uint8_t ReceivedMessageType = (inChar / 16) & 0x0F;
      uint8_t DUP = (inChar & DUP_Mask) / DUP_Mask;
      uint8_t QoS = (inChar & QoS_Mask) / QoS_Scale;
      uint8_t RETAIN = (inChar & RETAIN_Mask);
      if ((ReceivedMessageType >= CONNECT) && (ReceivedMessageType <= DISCONNECT))
      {
        bool NextLengthByte = true;
        MQTT.length = 0;
        MQTT.lengthLocal = 0;
        uint32_t multiplier=1;
        delay(2);
        char Cchar = inChar;
        while ( (NextLengthByte == true) && (MQTT.TCP_Flag == true))
        {
          if (gsmSerial.available())
          {
            inChar = (char)gsmSerial.read();

            #if ISASerial_needed
            ISASerial.println(inChar, DEC);
            #endif

            if ((((Cchar & 0xFF) == 'C') && ((inChar & 0xFF) == 'L') && (MQTT.length == 0)) || (((Cchar & 0xFF) == '+') && ((inChar & 0xFF) == 'P') && (MQTT.length == 0)))
            {
              MQTT.index = 0;
              GSMObj.gsmCapturedResponse[MQTT.index++] = Cchar;
              GSMObj.gsmCapturedResponse[MQTT.index++] = inChar;
              MQTT.TCP_Flag = false;
              MQTT.MQTT_Flag = false;
              MQTT.pingFlag = false;

              #if ISASerial_needed
              ISASerial.println(F("Disconnecting"));
              #endif

            }
            else
            {
              if ((inChar & 128) == 128)
              {
                MQTT.length += (inChar & 127) *  multiplier;
                multiplier *= 128;

                #if ISASerial_needed
                ISASerial.println(F("More"));
                #endif
              }
              else
              {
                NextLengthByte = false;
                MQTT.length += (inChar & 127) *  multiplier;
                multiplier *= 128;
              }
            }
          }
        }
        MQTT.lengthLocal = MQTT.length;


        #if ISASerial_needed
        ISASerial.print(F("MQTT packet length: "));
        ISASerial.println(MQTT.length);
        #endif


        if (MQTT.TCP_Flag == true)
        {
          MQTT.printMessageType(ReceivedMessageType);
          MQTT.index = 0L;
          uint32_t a = 0;
          while ((MQTT.length-- > 0) && (gsmSerial.available()))
          {
            GSMObj.gsmCapturedResponse[uint32_t(MQTT.index++)] = (char)gsmSerial.read();

            delay(1);

          }

          #if ISASerial_needed
          ISASerial.println(" ");
          #endif

          if (ReceivedMessageType == CONNACK)
          {
            MQTT.ConnectionAcknowledgement = GSMObj.gsmCapturedResponse[0] * 256 + GSMObj.gsmCapturedResponse[1];
            if (MQTT.ConnectionAcknowledgement == 0)
            {
              MQTT.MQTT_Flag = true;
              MQTT.OnConnect();

            }

            MQTT.printConnectAck(MQTT.ConnectionAcknowledgement);
            // MQTT.OnConnect();
          }
          else if (ReceivedMessageType == PUBLISH)
          {
            uint32_t TopicLength = (GSMObj.gsmCapturedResponse[0]) * 256 + (GSMObj.gsmCapturedResponse[1]);

            #if ISASerial_needed
            ISASerial.print(F("Topic : '"));
            #endif
            
            MQTT.PublishIndex = 0;
            for (uint32_t iter = 2; iter < TopicLength + 2; iter++)
            {
              #if ISASerial_needed
              ISASerial.print(GSMObj.gsmCapturedResponse[iter]);
              #endif

              MQTT.Topic[MQTT.PublishIndex++] = GSMObj.gsmCapturedResponse[iter];
            }
            MQTT.Topic[MQTT.PublishIndex] = 0;

            #if ISASerial_needed
            ISASerial.print(F("' Message :'"));
            #endif

            MQTT.TopicLength = MQTT.PublishIndex;

            MQTT.PublishIndex = 0;
            uint32_t MessageSTART = TopicLength + 2UL;
            int MessageID = 0;
            if (QoS != 0)
            {
              MessageSTART += 2;
              MessageID = GSMObj.gsmCapturedResponse[TopicLength + 2UL] * 256 + GSMObj.gsmCapturedResponse[TopicLength + 3UL];
            }
            for (uint32_t iter = (MessageSTART); iter < (MQTT.lengthLocal); iter++)
            {
              #if ISASerial_needed
              ISASerial.print(GSMObj.gsmCapturedResponse[iter]);
              #endif

              MQTT.Message[MQTT.PublishIndex++] = GSMObj.gsmCapturedResponse[iter];
            }
            MQTT.Message[MQTT.PublishIndex] = 0;

            #if ISASerial_needed
            ISASerial.println("'");
            #endif

            MQTT.MessageLength = MQTT.PublishIndex;
            if (QoS == 1)
            {
              MQTT.publishACK(MessageID);
            }
            else if (QoS == 2)
            {
              MQTT.publishREC(MessageID);
            }
            MQTT.OnMessage(MQTT.Topic, MQTT.TopicLength, MQTT.Message, MQTT.MessageLength);
            MQTT.MessageFlag = true;
          }
          else if (ReceivedMessageType == PUBREC)
          {
            #if ISASerial_needed
            ISASerial.print(F("Message ID :"));
            #endif

            MQTT.publishREL(0, GSMObj.gsmCapturedResponse[0] * 256 + GSMObj.gsmCapturedResponse[1]) ;

            #if ISASerial_needed
            ISASerial.println(GSMObj.gsmCapturedResponse[0] * 256 + GSMObj.gsmCapturedResponse[1]) ;
            #endif

          }
          else if (ReceivedMessageType == PUBREL)
          {

            #if ISASerial_needed
            ISASerial.print(F("Message ID :"));
            #endif

            MQTT.publishCOMP(GSMObj.gsmCapturedResponse[0] * 256 + GSMObj.gsmCapturedResponse[1]) ;

            #if ISASerial_needed
            ISASerial.println(GSMObj.gsmCapturedResponse[0] * 256 + GSMObj.gsmCapturedResponse[1]) ;
            #endif

          }
          else if ((ReceivedMessageType == PUBACK) || (ReceivedMessageType == PUBCOMP) || (ReceivedMessageType == SUBACK) || (ReceivedMessageType == UNSUBACK))
          {
            #if ISASerial_needed
            ISASerial.print(F("Message ID :"));
            ISASerial.println(GSMObj.gsmCapturedResponse[0] * 256 + GSMObj.gsmCapturedResponse[1]) ;
            #endif

          }
          else if (ReceivedMessageType == PINGREQ)
          {
            MQTT.TCP_Flag = false;
            MQTT.pingFlag = false;

            #if ISASerial_needed
            ISASerial.println(F("Disconnecting..."));
            #endif

            GSMObj.ISAgsmShut();
            MQTT.modemStatus = 0;
          }
        }
      }
      else if ((inChar = 13) || (inChar == 10))
      {
      }
      else
      {

        #if ISASerial_needed
        ISASerial.print(F("Received :Unknown Message Type :"));
        ISASerial.println(inChar);
        #endif
      }

  }
}