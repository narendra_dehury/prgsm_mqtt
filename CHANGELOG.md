#CHANGELOG
This is the changelog for "PRGSM_MQTT Library"

## v 1.1.0(2016-12-16)
### Added (PROCESS_MQTT.h)
1. pingStatus() to check in interval if still device is connected to server or not.


## v 1.0.0(2016-12-13)
### First Commit
1.PRGSM class for GSM TCP single continuous connection.
2.MQTT class for MQTT header and data handling.
3.MQTT_PROCESS for integration of both the classes. 